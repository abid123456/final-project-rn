import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

import { connect } from 'react-redux';
import { statsAssign, statsUpdate } from "../actions";

const apiLinksGlobal=[
  "https://api.countapi.xyz/get/abid-apps-etm/global-stats-accounts-created",
  "https://api.countapi.xyz/get/abid-apps-etm/global-stats-events-created",
  "https://api.countapi.xyz/get/abid-apps-etm/global-stats-events-deleted",
  "https://api.countapi.xyz/get/abid-apps-etm/global-stats-tasks-created",
  "https://api.countapi.xyz/get/abid-apps-etm/global-stats-tasks-deleted"
];

let apiLinks=[];

class StatsScreen extends React.Component {
  constructor(props) {
    super(props);
    
    if (this.props.route.params) {
      apiLinks=[
        ...apiLinksGlobal,
        `https://api.countapi.xyz/get/abid-apps-etm/${this.props.appState.credentials.userName}-stats-events-created`,
        `https://api.countapi.xyz/get/abid-apps-etm/${this.props.appState.credentials.userName}-stats-events-deleted`,
        `https://api.countapi.xyz/get/abid-apps-etm/${this.props.appState.credentials.userName}-stats-tasks-created`,
        `https://api.countapi.xyz/get/abid-apps-etm/${this.props.appState.credentials.userName}-stats-tasks-deleted`
      ]
    } else apiLinks=[...apiLinksGlobal];
    
    let a=[];
    for (let i=0; i<apiLinks.length; i++) {
      a[i]="...";
    }
    this.props.statsAssign(a);
    this.getStats();
  }
  
  getStats = async ()=>{
    let response=[];
    let json=[];
    
    for (let i=0; i<apiLinks.length; i++) {
      response[i]=await fetch(apiLinks[i]);
    }
    
    for (let i=0; i<apiLinks.length; i++) {
      json[i]=await response[i].json();
    }
    for (let i=0; i<apiLinks.length; i++) {
      this.props.statsUpdate({i,value:json[i].value});
    }
  }
  
  render() {
    if (!this.props.route.params) return (
      <View style={styles.container}>
        <View style={{marginTop:16, marginBottom:4}}>
          <Text style={styles.heading}>Worldwide</Text>
        </View>
        <View style={{flexDirection:'row'}}>
          <View style={{margin:8}}>
            <Text style={styles.contentText}>Accounts Created</Text>
            <Text style={styles.contentText}>Events Created</Text>
            <Text style={styles.contentText}>Events Deleted</Text>
            <Text style={styles.contentText}>Tasks Created</Text>
            <Text style={styles.contentText}>Tasks Deleted</Text>
          </View>
          <View style={{margin:8}}>
            <Text style={styles.contentNumber}>{this.props.stats[0]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[1]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[2]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[3]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[4]}</Text>
          </View>
        </View>
      </View>
    ); else return (
      <View style={styles.container}>
        <View style={{marginTop:16, marginBottom:4}}>
          <Text style={styles.heading}>Worldwide</Text>
        </View>
        <View style={{flexDirection:'row'}}>
          <View style={{margin:8}}>
            <Text style={styles.contentText}>Accounts Created</Text>
            <Text style={styles.contentText}>Events Created</Text>
            <Text style={styles.contentText}>Events Deleted</Text>
            <Text style={styles.contentText}>Tasks Created</Text>
            <Text style={styles.contentText}>Tasks Deleted</Text>
          </View>
          <View style={{margin:8}}>
            <Text style={styles.contentNumber}>{this.props.stats[0]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[1]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[2]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[3]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[4]}</Text>
          </View>
        </View>
        <View style={{marginTop:8, marginBottom:4}}>
          <Text style={styles.heading}>Account</Text>
        </View>
        <View style={{flexDirection:'row'}}>
          <View style={{margin:8}}>
            <Text style={styles.contentText}>Events Created</Text>
            <Text style={styles.contentText}>Events Deleted</Text>
            <Text style={styles.contentText}>Tasks Created</Text>
            <Text style={styles.contentText}>Tasks Deleted</Text>
          </View>
          <View style={{margin:8}}>
            <Text style={styles.contentNumber}>{this.props.stats[5]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[6]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[7]}</Text>
            <Text style={styles.contentNumber}>{this.props.stats[8]}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#bbceff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  statsButton: {
    height:36,
    borderRadius:4,
    justifyContent:'center',
    alignItems:'center',
    elevation:3,
    paddingLeft:16,
    paddingRight:16
  },
  heading: {
    fontSize:24,
    fontWeight:'bold'
  },
  contentText: {
    fontSize:18
  },
  contentNumber: {
    fontSize:18,
    textAlign:'right'
  }
});

function mapStateToProps(state) {
  return {
    stats: state.statistics,
    appState: state.appState
  };
}

export default connect(mapStateToProps, { statsAssign, statsUpdate })(StatsScreen);

