export default class DateTime {
  constructor(dateString, timeString) {
    [this.year,this.month,this.day]=[
      Number(dateString.slice(0,4)),
      Number(dateString.slice(4,6)),
      Number(dateString.slice(6,8))
    ];
    if (timeString != "-1") {
      [this.hour,this.minute]=[
        Number(timeString.slice(0,2)),
        Number(timeString.slice(2,4))
      ];
    } else [this.hour,this.minute]=[-1,-1]
    this.timeString=`${timeString.slice(0,2)}:${timeString.slice(2,4)}`;
  }
  
  monthName() {
    switch (this.month) {
      case  1: return "January";
      case  2: return "February";
      case  3: return "March";
      case  4: return "April";
      case  5: return "May";
      case  6: return "June";
      case  7: return "July";
      case  8: return "August";
      case  9: return "September";
      case 10: return "October";
      case 11: return "November";
      case 12: return "Desember";
    }
  }
  
  static eventTimeFormat(startDate,startTime,endDate,endTime) {
    let startDt=new DateTime(startDate,startTime);
    let endDt=new DateTime(endDate,endTime);
    if (startDt.year!=endDt.year) {
      return `${startDt.day} ${startDt.monthName()} ${startDt.year}`
            +`—${endDt.day} ${endDt.monthName()} ${endDt.year}`;
    } else if (startDt.month!=endDt.month) {
      return `${startDt.day} ${startDt.monthName()}`
            +`—${endDt.day} ${endDt.monthName()} ${endDt.year}`;
    } else if (startDt.day!=endDt.day) {
      return `${startDt.day}`
            +`—${endDt.day} ${endDt.monthName()} ${endDt.year}`;
    } else if (startDt.hour==-1) {
      return `${endDt.day} ${endDt.monthName()} ${endDt.year}`;
    } else {
      return `${endDt.day} ${endDt.monthName()} ${endDt.year}`
            +` ${startDt.timeString}—${endDt.timeString}`;
    }
  }
  
  static taskTimeFormat(endDate,endTime) {
    let endDt=new DateTime(endDate,endTime);
    return `${endDt.day} ${endDt.monthName()} ${endDt.year} ${endDt.timeString}`;
  }
}

