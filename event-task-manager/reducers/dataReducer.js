import { DATA_INIT, DATA_ASSIGN, DATA_REMOVE } from "../actions/actionTypes";

export default (state=0, action)=>{
  switch (action.type) {
    case DATA_INIT:
      return action.payload;
    case DATA_ASSIGN:
      {
        let newState={...state};
        let newStateBranch=newState[action.payload.branch];
        for (let i=0; i<newStateBranch.length; i++) {
          if (newStateBranch[i].id==action.payload.id) {
            newStateBranch[i]=action.payload.data;
            return newState;
          }
        }
        // didn't find item with id equal to payload.id,
        // creating new item
        newStateBranch[newStateBranch.length]=action.payload.data;
        return newState;
      }
    case DATA_REMOVE:
      {
        let newState={...state};
        let newStateBranch=newState[action.payload.branch];
        for (let i=0; i<newStateBranch.length; i++) {
          if (newStateBranch[i].id==action.payload.id) {
            newStateBranch.splice(i,1);
            return newState;
          }
        }
        return state;
      }
    default:
      return state;
  }
}

