import { STATS_ASSIGN, STATS_UPDATE } from "./actionTypes";

export const statsAssign =(rValue)=>{
  return {
    type: STATS_ASSIGN,
    payload: rValue
  };
}

export const statsUpdate =(updateInfo)=>{
  return {
    type: STATS_UPDATE,
    payload: updateInfo
  };
}

