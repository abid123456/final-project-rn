import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Switch } from 'react-native';

import { connect } from 'react-redux';
import { appStateSet, autofillToggle, autofillSet, dataInit, credentialSet } from "../actions";

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.props.appStateSet({
      autofill:false,
      credentials:{userName:"Abid",pin:"060601"}
    });
    this.props.dataInit({event:[],task:[]});
  }
  
  state={
    credentialsError:false
  }
  
  async loginHandler() {
    let fetchResponse=await fetch("https://api.countapi.xyz/get/abid-apps-etm/"+
      `pin-user-${this.props.appState.credentials.userName}`);
    let json=await fetchResponse.json();
    if (json.value==this.props.appState.credentials.pin) {
      this.setState({credentialsError:false});
      this.props.navigation.push('DrawerScreen');
    } else {
      this.setState({credentialsError:true});
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection:'row-reverse',marginTop:32,marginLeft:16}}>
          <Button
            title="About"
            onPress={()=>this.props.navigation.push('About',true)}
          />
        </View>
        <View style={{alignItems:'center'}}>
          <View style={{width:300,height:84,marginTop:36,marginBottom:42}}>
            <Text style={styles.titleText}>Events & Tasks Manager</Text>
          </View>
            <View style={styles.textBoxCaption}>
            <Text>Username</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={userName=>{this.props.credentialSet({kind:"userName",value:userName})}}
            value={this.props.appState.credentials?this.props.appState.credentials.userName:""}
          />
          <View style={styles.textBoxCaption}>
            <Text>PIN</Text>
          </View>
          <TextInput
            style={styles.textInput}
            secureTextEntry={true}
            onChangeText={pin=>{this.props.credentialSet({kind:"pin",value:pin})}}
            value={this.props.appState.credentials?this.props.appState.credentials.pin:""}
          />
          <View style={{flexDirection:'row',alignItems:'center',marginTop:8}}>
            <Switch
              title="Text"
              value={this.props.appState.autofill}
              onValueChange={this.props.autofillToggle}
            />
            <Text>Autofill credentials after logout</Text>
          </View>
          <Text style={{color:'red'}}>
            {this.state.credentialsError?"Invalid credentials":""}
          </Text>
          <View style={{flexDirection:'row',width:288,justifyContent:'space-around'}}>
            <Button
              title="Login"
              onPress={()=>this.loginHandler()}
            />
            <Button
              title="Register"
              onPress={()=>this.props.navigation.push
                ('Register Screen')}
            />
          </View>
          <StatusBar style="auto"/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D1D3FF',
    justifyContent: 'flex-start'
  },
  textInput: {
    width:288,
    height:36,
    borderWidth:1,
    backgroundColor:'white',
    paddingLeft:8,
    paddingRight:8
  },
  titleText: {
    fontSize:36,
    fontWeight:'bold',
    textAlign:'center'
  },
  textBoxCaption: {
    alignItems:'center',
    margin:4
  }
});

function map(state) {
  return {
    appState: state.appState,
    data:state.userData
  };
}

export default connect(map, { appStateSet, autofillToggle, autofillSet, dataInit, credentialSet })(LoginScreen);

