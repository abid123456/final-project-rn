import { DATA_INIT, DATA_ASSIGN, DATA_REMOVE } from "./actionTypes";

export const dataInit =(data)=>{
  return {
    type: DATA_INIT,
    payload: data
  };
}

export const dataAssign =(assignInfo)=>{
  return {
    type: DATA_ASSIGN,
    payload: assignInfo
  };
}

export const dataRemove =(id)=>{
  return {
    type: DATA_REMOVE,
    payload: id
  };
}

