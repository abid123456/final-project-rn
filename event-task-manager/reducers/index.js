import { combineReducers } from 'redux';
import AppStateReducer from "./appStateReducer";
import DataReducer from "./dataReducer";
import StatsReducer from "./statsReducer";

export default combineReducers({
  appState: AppStateReducer,
  userData: DataReducer,
  statistics: StatsReducer
});

