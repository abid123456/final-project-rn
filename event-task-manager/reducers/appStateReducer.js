import { AUTOFILL_TOGGLE, AUTOFILL_SET, APP_STATE_SET, CREDENTIAL_SET } from "../actions/actionTypes";

export default (state=0, action)=>{
  switch (action.type) {
    case AUTOFILL_TOGGLE:
      {
        let newState={...state};
        newState.autofill=!newState.autofill;
        return newState;
      }
    case AUTOFILL_SET:
      {
        let newState={...state};
        newState.autofill=action.payload;
        return newState;
      }
    case APP_STATE_SET:
      return action.payload;
    case CREDENTIAL_SET:
      {
        let newState={...state};
        newState.credentials[action.payload.kind]=action.payload.value;
        return newState;
      }
    default:
      return state;
  }
}

