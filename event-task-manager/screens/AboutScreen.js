import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, Image } from 'react-native';

export default class AboutScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{width:240,alignItems:'center',marginTop:48}}>
          <Text style={styles.screenTitle}>About the Developer</Text>
        </View>
        <View style={{width:96,height:96,margin:16}}>
          <Image
            source={require("../images/photo.png")}
            width={32}
          />
        </View>
        <Text style={styles.contentText}>Abdillah Ahmad</Text>
        <View style={{flexDirection:'row',marginTop:16}}>
          <View style={{margin:8}}>
            <Text style={styles.contentTextBold}>Facebook</Text>
            <Text style={styles.contentTextBold}>Twitter</Text>
            <Text style={styles.contentTextBold}>Instagram</Text>
          </View>
          <View style={{margin:8}}>
            <Text style={styles.contentText}>Abdillah Ahmad</Text>
            <Text style={styles.contentText}>@KucingMiaw1</Text>
            <Text style={styles.contentText}>@thereconnoitrer1716</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',marginBottom:16}}>
          <View style={{margin:8}}>
            <Text style={styles.contentTextBold}>Github</Text>
            <Text style={styles.contentTextBold}>Gitlab</Text>
          </View>
          <View style={{margin:8}}>
            <Text style={styles.contentText}>abid123456</Text>
            <Text style={styles.contentText}>abid123456</Text>
          </View>
        </View>
        <TouchableOpacity
          style={{...styles.statsButton, backgroundColor:"#f0bb33"}}
          onPress={()=>{
            this.props.navigation.push('App Statistics',this.props.route.params?false:true);
          }}
        >
          <Text style={styles.contentText}>App Statistics</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.statsButton, backgroundColor:"#ff9900", marginTop:8}}
          onPress={()=>{
            this.props.navigation.pop();
          }}
        >
          <Text style={styles.contentText}>Back</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#bbceff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  statsButton: {
    height:36,
    borderRadius:4,
    justifyContent:'center',
    alignItems:'center',
    elevation:3,
    paddingLeft:16,
    paddingRight:16
  },
  screenTitle: {
    fontSize:36,
    fontWeight:'bold',
    textAlign:'center'
  },
  contentText: {
    fontSize:18
  },
  contentTextBold: {
    fontSize:18,
    fontWeight:'bold'
  }
});

