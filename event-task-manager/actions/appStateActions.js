import { AUTOFILL_TOGGLE, AUTOFILL_SET, APP_STATE_SET, CREDENTIAL_SET } from "./actionTypes";

export const autofillToggle =()=>{
  return {
    type: AUTOFILL_TOGGLE
  };
}

export const autofillSet =(newState)=>{
  return {
    type: AUTOFILL_SET,
    payload: newState
  };
}

export const appStateSet =(newState)=>{
  return {
    type: APP_STATE_SET,
    payload: newState
  };
}

export const credentialSet =(credentialInfo)=>{
  return {
    type: CREDENTIAL_SET,
    payload: credentialInfo
  };
}

