import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, TouchableOpacity, TextInput } from 'react-native';

import dataSource from "../data.json";
import DateTime from "../class/DateTime";

import { connect } from 'react-redux';
import { dataAssign } from "../actions";

class TaskDetails extends React.Component {
  constructor (props) {
    super(props);
    if (!this.props.route.params) {
      this.state.workingData={
        id:smallestAvailableId(this.props.data.task).toString(),
        taskname:"New task",
        enddate:"01/01/2020",
        endtime:"01:00",
      };
    } else {
      this.state.workingData=this.extractData(this.props.route.params);
    }
  }
  
  state={
    workingData:null
  }
  
  extractData(data) {
    let result={...data};
    result.enddate=
      `${data.enddate.slice(6,8)}/${data.enddate.slice(4,6)}/${data.enddate.slice(0,4)}`;
    result.endtime=
      `${data.endtime.slice(0,2)}:${data.endtime.slice(2,4)}`
    return result;
  }
  
  reformData(data) {
    let result={...data};
    result.enddate=
      `${data.enddate.slice(6,10)}${data.enddate.slice(3,5)}${data.enddate.slice(0,2)}`;
    result.endtime=
      `${data.endtime.slice(0,2)}${data.endtime.slice(3,5)}`
    return result;
  }
  
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems:'center',padding:36}}>
          <View>
            <Text style={{fontSize:36, fontWeight:'bold'}}>
              {!this.props.route.params?
              "Create Task":"Task Details"}
            </Text>
          </View>
        </View>
        <View>
          <Text>Task Name</Text>
        </View>
        <TextInput
          style={styles.taskNameInput}
          onChangeText={(taskName)=>
            {this.setState({workingData:
            {...this.state.workingData,taskname:taskName}})}}
          value={this.state.workingData.taskname}
        />
        <View style={{flexDirection:'row',alignItems:'flex-end'}}>
          <View style={{margin:14}}>
            <Text>Deadline Date</Text>
            <Text>(dd/mm/yyyy)</Text>
            <TextInput
              style={styles.dateInput}
              onChangeText={(endDate)=>
                {this.setState({workingData:
                {...this.state.workingData,enddate:endDate}})}}
              value={this.state.workingData.enddate}
            />
          </View>
          <View style={{margin:14}}>
            <Text>Time (hh:mm)</Text>
            <TextInput
              style={styles.timeInput}
              onChangeText={(endTime)=>
                {this.setState({workingData:
                {...this.state.workingData,endtime:endTime}})}}
              value={this.state.workingData.endtime}
            />
          </View>
        </View>
        <View style={{flexDirection:'row',marginTop:36}}>
          <TouchableOpacity
            style={{...styles.exitButton, backgroundColor:"#ff9900"}}
            onPress={()=>{
              this.props.navigation.pop();
            }}
          >
            <Text>Cancel</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...styles.exitButton, backgroundColor:"#007df0"}}
            onPress={()=>{
              let reformedData = this.reformData(this.state.workingData);
              let currentData = this.props.route.params;
              this.props.dataAssign({branch:"task",id:currentData.id,data:reformedData});
              if (!this.props.route.params) {
                fetch("https://api.countapi.xyz/hit/abid-apps-etm/global-stats-tasks-created");
                fetch("https://api.countapi.xyz/hit/abid-apps-etm/"
                  +`${this.props.appState.credentials.userName}-stats-tasks-created`);
              }
              this.props.navigation.pop();
            }}
          >
            <Text>OK</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function smallestAvailableId(data) {
  for (let id=1; id<data.length+2; id++) {
    let idFound = false;
    for (let i=0; i<data.length; i++) {
      if (data[i].id==id) {
        idFound=true;
        break;
      }
    }
    if (!idFound) return id;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#aaceff',
    alignItems: 'center'
  },
  taskNameInput: {
    width:288,
    height:36,
    borderWidth:1,
    backgroundColor: 'white',
    paddingLeft:8,
    paddingRight:8
  },
  dateInput: {
    width:129,
    height:36,
    borderWidth:1,
    backgroundColor: 'white',
    paddingLeft:8,
    paddingRight:8
  },
  timeInput: {
    width:76,
    height:36,
    borderWidth:1,
    backgroundColor: 'white',
    marginTop:2,
    paddingLeft:8,
    paddingRight:8
  },
  exitButton: {
    height:32,
    borderRadius:4,
    justifyContent:'center',
    alignItems:'center',
    elevation:3,
    width:130,
    marginLeft:8,
    marginRight:8
  }
});

function mapStateToProps(state) {
  return {
    data: state.userData,
    appState: state.appState
  };
}

export default connect(mapStateToProps, { dataAssign })(TaskDetails);

