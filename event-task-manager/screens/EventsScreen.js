import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, TouchableOpacity, AsyncStorage } from 'react-native';

import { Ionicons } from '@expo/vector-icons';

import dataSource from "../data.json";
import DateTime from "../class/DateTime";

import { connect } from 'react-redux';
import { dataInit, dataRemove } from "../actions";

class EventsScreen extends React.Component {
  constructor (props) {
    super(props);
    
    if (this.props.data.event.length==0
      && this.props.data.task.length==0) {
      ;//this.props.dataInit(dataSource);
    }
    this.state.deleting=false;
  }
  
  state={
    deleting:null
  }
  
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems:'center',padding:36}}>
          <View>
            <Text style={{fontSize:36, fontWeight:'bold'}}>
              {!this.state.deleting?`${this.props.appState.credentials.userName}'s Events`:"Deleting Events"}
            </Text>
          </View>
        </View>
        <View style={{flex:1}}>
          <FlatList
            data={this.props.data.event}
            renderItem={({item}) => <Event data={item} navigation={this.props.navigation} renderer={this}/>}
            keyExtractor={item=>item.id}
          />
        </View>
          <View style={{flexDirection:'row-reverse'}}>
          <TouchableOpacity
            style={{...styles.controlButton, width:32, backgroundColor:"#f0bb33"}}
            onPress={()=>{
              this.setState({deleting:false});
              this.props.navigation.push("Event Details",false);
            }}
          >
            <Ionicons name="md-add" size={20} color='white'/>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...styles.controlButton, width:32, backgroundColor:"#f03333"}}
            onPress={()=>this.setState({deleting:!this.state.deleting})}
          >
            <Ionicons name="md-trash" size={20} color='white'/>
          </TouchableOpacity>
        </View>
        <StatusBar style="auto" />
      </View>
    );
  }
}

class Event extends React.Component {
  itemTimeFormat(itemData) {
    result =DateTime.eventTimeFormat
            (itemData.startdate,itemData.starttime,
            itemData.enddate,itemData.endtime);
    return result;
  }
  
  render() {
    const itemData=this.props.data;
    return (
      <TouchableOpacity
        style={styles.eventView}
        onPress={()=>{
          if (!this.props.renderer.state.deleting) {
            this.props.navigation.push("Event Details",this.props.data);
          } else {
            this.props.renderer.props.dataRemove
              ({branch:"event",id:this.props.data.id});
            fetch("https://api.countapi.xyz/hit/abid-apps-etm/global-stats-events-deleted");
            fetch("https://api.countapi.xyz/hit/abid-apps-etm/"
              +`${this.props.renderer.props.appState.credentials.userName}-stats-events-deleted`);
          }
        }}
      >
        <Text style={styles.eventName}>{itemData.eventname}</Text>
        <Text style={styles.eventDetails}>
          {this.itemTimeFormat(itemData)}
        </Text> 
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#aaceff'
  },
  eventView: {
    marginTop:4,
    marginBottom:4,
    marginLeft:8,
    marginRight:8,
    padding:4,
    backgroundColor:'#3fcb98',
    elevation:3
  },
  eventName: {
    fontSize: 18,
    fontWeight: 'bold',
    color:'#1f6239'
  },
  eventDetails: {
    fontSize: 14,
    fontWeight: 'bold',
    color:'#1f6239'
  },
  controlButton: {
    height:32,
    borderRadius:4,
    justifyContent:'center',
    alignItems:'center',
    elevation:3,
    marginTop:8,
    marginLeft:4,
    marginRight:4,
    marginBottom:8
  }
});

function mapStateToProps(state) {
  return {
    data: state.userData,
    appState: state.appState
  };
}

export default connect(mapStateToProps, { dataInit, dataRemove })(EventsScreen);

