import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Switch } from 'react-native';

import { connect } from 'react-redux';
import { credentialSet } from "../actions";

class LogoutScreen extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.appState.autofill) {
      ;
    } else {
      this.props.credentialSet({kind:"userName",value:''});
      this.props.credentialSet({kind:"pin",value:''});
    }
    this.props.navigation.popToTop();
  }
  
  render() {
    return (
      <View/>
    );
  }
}

function map(state) {
  return {
    appState: state.appState,
    data:state.userData
  };
}

export default connect(map, { credentialSet })(LogoutScreen);

