import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import { Ionicons } from '@expo/vector-icons';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers/index';
const store = createStore(reducers);

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import EventsScreen from "./screens/EventsScreen";
import EventDetails from "./screens/EventDetails";
import TasksScreen from "./screens/TasksScreen";
import TaskDetails from "./screens/TaskDetails";
import AboutScreen from "./screens/AboutScreen";
import StatsScreen from "./screens/StatsScreen";
import LogoutScreen from "./screens/LogoutScreen";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const DrawerScreen =()=>(
  <Drawer.Navigator>
    <Drawer.Screen name='Home' component={HomeScreen}/>
    <Drawer.Screen name='About' component={AboutScreen}/>
    <Drawer.Screen name='Logout' component={LogoutScreen}/>
  </Drawer.Navigator>
);

const HomeScreen =()=>(
  <Tabs.Navigator
    screenOptions = {({route})=>({
      tabBarIcon: ({focused,color,size}) => {
        let iconName;
        switch(route.name) {
          case 'Events':
            iconName='md-calendar';
            break;
          case 'Tasks':
            iconName='md-clipboard';
            break;
          default:
            iconName='md-close'
            break;
        }
        return <Ionicons name={iconName} size={24} color={color}/>
      }
    })}
  >
    <Tabs.Screen name='Events' component={EventsScreen}/>
    <Tabs.Screen name='Tasks' component={TasksScreen}/>
  </Tabs.Navigator>
);

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='LoginScreen'>
          <Stack.Screen
            name='Login Screen'
            component={LoginScreen}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='Register Screen'
            component={RegisterScreen}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='DrawerScreen'
            component={DrawerScreen}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='Event Details'
            component={EventDetails}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='Task Details'
            component={TaskDetails}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='About'
            component={AboutScreen}
            options={{headerShown:false}}
          />
          <Stack.Screen
            name='App Statistics'
            component={StatsScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

