import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

import { connect } from 'react-redux';
import { credentialSet } from "../actions";

class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  
  state={
    confirmationPin:""
  }
  
  async registerHandler() {
    if (this.props.appState.credentials.pin != this.state.confirmationPin) {
      alert("PIN and confirmation do not match");
      return;
    }
    let fetchResponse =
      await fetch("https://api.countapi.xyz/create?namespace=abid-apps-etm"
      +`&key=pin-user-${this.props.appState.credentials.userName}`
      +`&value=${this.props.appState.credentials.pin}`
      +"&enable_reset=1");
    let json=await fetchResponse.json();
    if (json.key==`pin-user-${this.props.appState.credentials.userName}`) {
      fetch("https://api.countapi.xyz/hit/abid-apps-etm/global-stats-accounts-created");
      alert("Account successfully created");
      this.props.navigation.pop();
    } else {
      alert("Account creation failed (username taken or other problem)");
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems:'center'}}>
          <View style={{width:300,height:84,marginTop:100}}>
            <Text style={styles.titleText}>Create Account</Text>
          </View>
          <View style={styles.textBoxCaption}>
            <Text>Username</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={userName=>{this.props.credentialSet({kind:"userName",value:userName})}}
            value={this.props.appState.credentials?this.props.appState.credentials.userName:""}
          />
          <View style={styles.textBoxCaption}>
            <Text>PIN (6 digits)</Text>
          </View>
          <TextInput
            style={styles.textInput}
            secureTextEntry={true}
            onChangeText={pin=>{this.props.credentialSet({kind:"pin",value:pin})}}
            value={this.props.appState.credentials?this.props.appState.credentials.pin:""}
          />
          <View style={styles.textBoxCaption}>
            <Text>Confirm PIN</Text>
          </View>
          <TextInput
            style={styles.textInput}
            secureTextEntry={true}
            onChangeText={pin=>{this.setState({confirmationPin:pin})}}
            value={this.state.confirmationPin}
          />
          <View style={{flexDirection:'row',width:288,justifyContent:'space-around',marginTop:32}}>
            <Button
              title="Register"
              onPress={()=>this.registerHandler()}
            />
            <Button
              title="Back to Login"
              onPress={()=>this.props.navigation.pop()}
            />
          </View>
          <StatusBar style="auto"/>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D1D3FF',
    justifyContent: 'flex-start'
  },
  textInput: {
    width:288,
    height:36,
    borderWidth:1,
    backgroundColor:'white',
    paddingLeft:8,
    paddingRight:8
  },
  titleText: {
    fontSize:36,
    fontWeight:'bold',
    textAlign:'center'
  },
  textBoxCaption: {
    alignItems:'center',
    margin:4
  }
});

function map(state) {
  return {
    appState: state.appState
  };
}

export default connect(map, { credentialSet })(RegisterScreen);

