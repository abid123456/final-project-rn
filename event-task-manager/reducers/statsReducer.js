import { STATS_ASSIGN, STATS_UPDATE } from "../actions/actionTypes";

export default (state=0, action)=>{
  switch (action.type) {
    case STATS_ASSIGN:
      return action.payload;
    case STATS_UPDATE:
      newState=[...state];
      newState[action.payload.i]=action.payload.value;
      return newState;
    default:
      return state;
  }
}

